#FROM tomcat:8.0.20-jre8
### Good stuff
#COPY target/*.war /usr/local/tomcat/webapps/maven-web-app.war




FROM tomcat:8.0.20-jre8
WORKDIR /app
COPY target/*.war .
EXPOSE 8080
CMD ["java", "-war", "*.war"]

